import { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {Link} from "react-router-dom";
import AuthService from '../../services/auth-services';

function Header()  {
    
  const [currentUser, setCurrentUser] = useState(undefined);
//  const userData = useContext(UserContext);
    const logOut = () => { 
        
         AuthService.logout();
    };
    
    useEffect(() => {
 
      const user = AuthService.getCurrentUser();
    console.log("Use Effect" )
        if (user) {
          setCurrentUser(user);
             
             
        }

      }, []);


  return (
    <div>
    <nav className="navbar navbar-expand navbar-dark bg-dark">
        <Link to={"/"}
            className="navbar-brand">
            SuperProcure
        </Link>
        <div className="navbar-nav  mr-auto">
            <li className="nav-item">
                <Link to={"/home"}
                    className="nav-link">
                    Home
                </Link>
            </li>
           
</div>
            {
            currentUser ? (
                <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to={"/branch/currentUser.BranchId"}
                            className="nav-link">
                            {
                            currentUser.BranchId
                        } </Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/login"} className="nav-link" >
                                <button onClick={logOut} > LogOut</button>
                            
                        </Link>
                    </li>
                </div>
            ) : (
                <div className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <Link to={"/customer"}
                            className="nav-link">
                            Find Nearest Store
                        </Link>
                    </li>
                    <li className="nav-item"  >
                        <Link to={"/login"}
                            className="nav-link" >
                                <button>
                            Login</button>
                        </Link>
                    </li>

                    
                </div>
            )
        } </nav> </div>
  );
};

export default Header;
