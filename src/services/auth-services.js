import axios from "axios";
import {getConfiguration} from '../config/config';


const API_URL = getConfiguration().authUrl + "/v1/auth/";

const register = (username, email, password, role) => {
    return axios.post(API_URL + "register/", {username, email, password, role});
};
const loginRedirect = (data) => {


    if (data && data.error !== undefined) {

        if (data.statusCode === 401 && data.error === 'Unauthorized') {

            sessionStorage.clear();
            redirectIfNoSession();
        }
    }
}
const redirectIfNoSession = () => {
    let jwt = sessionStorage.getItem('user');
    if (! jwt || jwt === 'undefined' || jwt === '') {
        window.location = '/login';
    }
}

const login = (userId, password) => {
    return axios.post(API_URL + "login/", {userId, password}).then((response) => {
      //  if (response.data.accessToken) {
        if (response.data) {
            sessionStorage.setItem("user", JSON.stringify(response.data));
        }

        return response.data;
    });
};
const changePassword = (userObjId, oldPassword,newPassword,confirmNewPassword) => {
    return axios.post(API_URL + "login/"+userObjId+"/", {oldPassword, newPassword,confirmNewPassword}).then((response) => {
        
        return response.data;
    });
};

const logout = () => {
    sessionStorage.removeItem("user");
    sessionStorage.clear();
};

const getCurrentUser = () => {
    return JSON.parse(sessionStorage.getItem("user"));
};

const AuthService = {
    register,
    login,
    logout,
    getCurrentUser,
    loginRedirect,
    redirectIfNoSession,
    changePassword
};
export default AuthService;
