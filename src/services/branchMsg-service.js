import axios from "axios";
import { getConfiguration } from '../config/config';
import AuthHeader from "./auth-header";

const API_URL = getConfiguration().apiUrl+"/v1/sp/branch/";

  
 async function getBranchMsgByBranchId (branchId) {
    try {
        const response = await axios.get(API_URL+branchId+'/message/',{headers:AuthHeader.authHeader()})
            return response.data;

    }catch(error) {
        throw error;
    }
};
async function getAllBranchMsg () {
    try {
        const response = await axios.get(API_URL+ 'message/',{headers:AuthHeader.authHeader()})
            return response.data;

    }catch(error) {
        throw error;
    }
};
async function updateBranchMsgByMsgId (branchMsgId,branchMsgData) {
    try {
        const response = await axios.put(API_URL+"message/"+branchMsgId+'/',branchMsgData,{headers:AuthHeader.authHeader()
        })
        return response.data;
    }catch(error) {
        throw error;
    }
};

const BranchMsgService = {
    getBranchMsgByBranchId,
    updateBranchMsgByMsgId,
    getAllBranchMsg

}
export default BranchMsgService;
