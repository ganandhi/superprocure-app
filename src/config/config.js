const serverVars = {
    authUrl: '#{authUrl}#',
    apiUrl: '#{apiUrl}#'
};

const localVars = {
    authUrl: 'http://localhost:7000',
    apiUrl: 'http://localhost:7000'

};

export function getConfiguration() {
    if (process.env.NODE_ENV === 'production') {
        return serverVars;
    }


    return localVars;
}
