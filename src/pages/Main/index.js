import { Switch, Route} from "react-router-dom";
import Login from '../Login';
import Branch from '../Branch';
import Customer from '../Customer';
import Home from '../Home';

import Header from '../../layouts/Header';
function Main() {
   return (
      <div>
                                     
            <Switch> 
                     
              <Route exact path={["/", "/home"]} > <Header /><Home /></Route>
              <Route exact path="/login" > <Header /><Login  /></Route>
             <Route exact path="/branch/:branchId/" > <Header /><Branch  /></Route>
             <Route exact path="/customer/"  > <Header /><Customer  /></Route>
             </Switch>
            
            </div>

        );
        }

export default Main;