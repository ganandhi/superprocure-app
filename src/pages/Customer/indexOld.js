import React, { useState, useEffect } from "react";
import socketIOClient from "socket.io-client";
const ENDPOINT = "http://localhost:7000";
const CustomerOld= () => {

  const [response, setResponse] = useState("");
  const socket = socketIOClient(ENDPOINT);

  useEffect(() => {

    console.log("Inside Customer Effect");
    
    socket.on("FromAPI", data => {
      console.log(data);
      setResponse(data);
    });
  }, []);
    return (
      <div className="jumbotron text-center">
          
        <h2 > Customer</h2>
        <p>
      It's {response}
    </p>
        </div>
        
    
    );
  };
  
  export default CustomerOld;