import { useState,useEffect, useRef ,useContext} from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
//import {UserContext} from "../../App"
//import AuthService from "../../services/auth-services";
import { useHistory} from "react-router-dom";
import socketIOClient from "socket.io-client";
const ENDPOINT = "http://localhost:7000";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Customer = (props) => {
 // const user=useContext(UserContext);


  const form = useRef();
  const checkBtn = useRef();

  const [phoneno, setPhoneno] = useState("");
  const [pincode, setPincode] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  //const [res,setRes] = useState({});
 // const [rerender,setReRender] = useState(false);
//  const history = useHistory()

  const [response, setResponse] = useState([]);
  const socket = socketIOClient(ENDPOINT);

  useEffect(() => {

    console.log("Inside Customer Effect");
    
    socket.on(phoneno, data => {
      console.log(data);
      setResponse(data);
    });
  }, []);

 

  const onChangePhoneno = (e) => {
    const phoneno = e.target.value;
    setPhoneno(phoneno);
    
    
  };

  const onChangePincode = (e) => {
    const pincode = e.target.value;
    setPincode(pincode);
  };
  /* const [toDashboard, setToDashboard] = useState(false)

  if (toDashboard === true) {
    return <Redirect to='/packages/'/>
  } */

  const handleSubmit = (e) => {
    e.preventDefault();

    setMessage("");
    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      const data ={"pincode":pincode,"phoneno":phoneno}
     socket.emit('FindBranch',data)
    } else {
      setLoading(false);
    }
  };
 

  return (
    <div className="col-md-12">
      <div className="card card-container">
        <img
          src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
          alt="profile-img"
          className="profile-img-card"
        />

        <Form onSubmit={handleSubmit}   ref={form}>
          <div className="form-group">
            <label htmlFor="phoneno">Phoneno</label>
            <Input
              type="text"
              className="form-control"
              name="phoneno"
              value={phoneno}
              onChange={onChangePhoneno}
              validations={[required]}
            />
          </div>

          <div className="form-group">
            <label htmlFor="pincode">Pincode</label>
            <Input
              type="text"
              className="form-control"
              name="pincode"
              value={pincode}
              onChange={onChangePincode}
              validations={[required]}
            />
          </div>

          <div className="form-group">
            <button className="btn btn-primary btn-block" disabled={loading}>
              {loading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}
              <span>Submit</span>
            </button>
          </div>

          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
      {response.map(data => JSON.stringify(data))}
    </div>
  );
};

export default Customer;
