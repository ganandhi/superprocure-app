import { useState,useEffect } from 'react';
import BranchMsgView from './BranchMsgView';
import { useParams} from 'react-router-dom';
function Branch() {

    const [dummyState, rerender] = useState(1);
    let {branchId} = useParams();

    useEffect(() => {
        console.log("dummyState's state has updated to: " + dummyState)
        
    }, [dummyState])
    
    function handleReLoad(newValue) {
        rerender(dummyState + newValue);

    }
    return <BranchMsgView key={dummyState}
    reLoader={handleReLoad} BranchId={branchId}/>
    
}
export default Branch
