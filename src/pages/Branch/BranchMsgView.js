import { useMemo, Fragment, useState, useEffect,useCallback} from 'react';
import styled from 'styled-components'
import {useTable} from 'react-table'
import authService from "../../services/auth-services"
import branchMsgService from '../../services/branchMsg-service';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PopupState, {bindTrigger, bindMenu} from 'material-ui-popup-state';
import {Button} from '@material-ui/core';



const Styles = styled.div `
  padding: 1rem;

  table {
    border-spacing: 0;
    border: 1px solid black;

    tr {
      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px solid black;
      border-right: 1px solid black;

      :last-child {
        border-right: 0;
      }
    }
  }
  
`

function Table({columns, data}) {
//    let history = useHistory();
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow
    } = useTable({columns, data})
   
   /*  const addBranchMsgJS = (branchMsgId) => {
        

        history.push("/branchMsg/addEdit_branchMsg/");
     //   window.location.reload();

    } */


    // Render the UI for your table
    return (
        <div>
            <p>
                Notification Centre
            </p>
            <table {...getTableProps()}>
                <thead> {
                    headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {
                            headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps()}>
                                    {
                                    column.render('Header')
                                }</th>
                            ))
                        } </tr>
                    ))
                } </thead>
                <tbody {...getTableBodyProps()}>
                    {
                    rows.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {
                                row.cells.map(cell => {
                                    return <td {...cell.getCellProps()}>
                                        {
                                        cell.render('Cell')
                                    }</td>
                            })
                            } </tr>
                        )
                    })
                } </tbody>
            </table>
        </div>
    )
}

function BranchMsgView(props) {
    const [branchMsg, setBranchMsg] = useState([]);   
    const [hasErr, setErrs] = useState(false);
    const [errMsg, setErrMsg] = useState({});
    
    const {reLoader} = props;

    const updateBranchMsgJS = useCallback((branchMsgId,msgStatus) => {
        
        setErrMsg({});
        setErrs(false);

        branchMsgService.updateBranchMsgByMsgId(branchMsgId,msgStatus).then(res => { // setSpecies([]);
           // window.location.reload();
           reLoader(1);
        }).catch(err => {
            
            
            setErrs(true);
            setErrMsg(err);
            authService.loginRedirect(err.response.data);

        });


    },[reLoader])
    /* const updateBranchMsgJS = useCallback((branchMsgId) => {

      
        history.push("/branchMsg/add_branchMsg/" + branchMsgId + "/");
     //   window.location.reload();

        // deletePackage(pkgId);

    },[history]);
 */

    useEffect(() => { 
        setErrMsg({});
        setErrs(false);
       // setBranchMsg([]);
        branchMsgService.getBranchMsgByBranchId(props.BranchId).then(res => {
            
            setBranchMsg(res);
        }).catch(err => {
            
            
            setErrs(true);
            setErrMsg(err);
            authService.loginRedirect(err.response);

        });

    }, [props.BranchId])
    const columns = useMemo(() => [


        {   
            Header: 'Branch Message Id',
            accessor: '_id',
            Cell: ({row}) => <div>   <td>             <PopupState variant="popover" popupId="demo-popup-menu">
            {(popupState) => (
              <Fragment>
                <Button variant="outlined" color="primary" {...bindTrigger(popupState)}>
                  ...
                </Button>
                <Menu {...bindMenu(popupState)}>
               
                  <MenuItem onClick = {() =>  {popupState.close(); updateBranchMsgJS(`${row.original._id}`)}}>Edit</MenuItem>
                  <MenuItem onClick = {() => {popupState.close();  updateBranchMsgJS(`${row.original._id}`,'PROCESSED')}}>Processed</MenuItem>
                <MenuItem onClick = {() => {popupState.close();  updateBranchMsgJS(`${row.original._id}`,'CLOSED')}}>Closed</MenuItem>
              <MenuItem onClick = {() => {popupState.close();  updateBranchMsgJS(`${row.original._id}`,'SUBMITTED')}}>Send for Processing</MenuItem>

                
                 </Menu>
                      
              </Fragment>
            )}
          </PopupState></td>
         <td> {row.original._id}</td>
      </div>,
            filterable: true,
            width: '300'
        },
       
              {
                Header: ' BranchId',
                accessor: 'BranchId',
              },
              {
                Header: 'PinCode',
                accessor: 'PinCode',
              },
              {
                Header: 'Status',
                accessor: 'Status',
              },
              {
                Header: 'CustomerMobile',
                accessor: 'CustomerMobile',
              },
              {
                Header: 'createdDate',
                accessor: 'createdDate',
              }
        

          ,],[updateBranchMsgJS] )

    const data = useMemo(() => branchMsg,[branchMsg])

    return (
        <Styles>
            <div> {
                hasErr && <div style={
                    {color: `red`}
                }>Some error occurred, while fetching api {
                    errMsg.error
                }</div>
            } </div>
            <Table columns={columns}
                data={data}/>
        </Styles>
    )
}

export default BranchMsgView;
