import "./App.css";

import React,{useState} from "react";
import Main from './pages/Main'

export const UserContext = React.createContext();


function App() {
  const [con,setCon] = useState({ id: "",
    username:"",
    email: "",
    role:"",
    accessToken: ""});
 
function populateUserData(newName) {
  setCon({...con, id: newName._id,
    username: newName.username,
    email: newName.email,
    role:newName.role,
    accessToken: newName.accessToken});
}

  const val = {
    getter:con,
    setter:populateUserData
  }
 
    return (
      <div>
         <UserContext.Provider value={val}>  
                   
           <Main ></Main>
            </UserContext.Provider>
            </div>

        );
        }

export default App;
